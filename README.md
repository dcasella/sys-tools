# System administration Tools

Automation or facilitation of administration tasks.

## Scripts

### Hosts file manager

```
manage-hosts.sh
```

Add or update entries in a "IP FQDN Hostname" file (i.e. /etc/hosts).

#### Download

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/manage-hosts.sh --output manage-hosts.sh; chmod +x manage-hosts.sh
```

#### Usage examples

```bash
# Verbose dry-run: add two hosts to /etc/hosts
./manage-hosts.sh -dv -p 10.1.2.3=other-host -p 10.1.2.4=host.domain.tld /etc/hosts
```

```bash
# Replace existing IPs with the updated FQDN and Hostname and add new ones
./manage-hosts.sh -r -p 127.0.0.1=localhost -p 10.1.1.3=host.domain.tld /etc/hosts
```
