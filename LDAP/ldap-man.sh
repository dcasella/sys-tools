#!/usr/bin/env bash

green() {
	tput setaf 2
}
yellow() {
	tput setaf 3
}
magenta() {
	tput setaf 5
}
cyan() {
	tput setaf 6
}
gray() {
	tput setaf 8
}
ul() {
	local text=$*

	tput smul; echo -n "${text}"; tput rmul
}
reset() {
	tput sgr0
}
v() {
	gray; printf '│'; reset
}
h() {
	gray; printf '─'; reset
}
hv() {
	gray; printf '┬'; reset
}
i() {
	gray; printf '┼'; reset
}
vh() {
	gray; printf '┴'; reset
}

repeat_h() {
	local repeat=$1

	printf -v str '%*s' "${repeat}" ''

	gray
	echo -n "${str// /─}"
	reset
}

fill_h() {
	local minus=${1:-0}

	repeat_h $(( $(tput cols) - minus ))
}

CONFIG=/etc/ldap.cfg

case "${1}" in
	-f | --file)
		CONFIG="${2}"
		shift 2
		;;
esac

source "${CONFIG}" || exit 1

man_ldapsearch() {
	cat <<- EOD
	$(h)$(hv)$(fill_h 2)
	 $(v) Command $(ul ldapsearch)
	$(h)$(vh)$(repeat_h 15)$(hv)$(fill_h 18)
	   -LLL          $(v) ldif format simplification
	   -b $(ul searchbase) $(v)
	   -D $(ul binddn)     $(v)
	   -W            $(v) prompt password
	   -y $(ul passwdfile) $(v)
	   -H $(ul ldapuri)    $(v)
	   FILTER        $(v) default: $(yellow)"(objectClass=*)"$(reset)
	$(repeat_h 17)$(vh)$(fill_h 18)
	 $(gray)# Prompt for password$(reset)
	 $(green)ldapsearch $(cyan)-LLL -b $(yellow)"${LDAP_USERDN}" $(cyan)-D $(yellow)"${LDAP_BINDUSER}" $(cyan)-W -H $(yellow)"${LDAP_URI}" $(cyan)-o$(reset) ldif-wrap=no FILTER [ATTRIBUTE]
	$(fill_h)
	 $(gray)# Use passwd file$(reset)
	 $(green)ldapsearch $(cyan)-LLL -b $(yellow)"${LDAP_USERDN}" $(cyan)-D $(yellow)"${LDAP_BINDUSER}" $(cyan)-y $(magenta)/etc/ldap-password $(cyan)-H $(yellow)"${LDAP_URI}" $(cyan)-o$(reset) ldif-wrap=no FILTER [ATTRIBUTE]
	$(fill_h)
	EOD
}

man_ldapmodify() {
	cat <<- EOD
	$(h)$(hv)$(fill_h 2)
	 $(v) Command $(ul ldapmodify)
	$(h)$(vh)$(repeat_h 15)$(hv)$(fill_h 18)
	   -n            $(v) dry-run
	   -v            $(v) verbose
	   -a            $(v) add entries
	   -f $(ul file)       $(v) ldif file
	   -x            $(v) simple auth (no SASL)
	   -D $(ul binddn)     $(v)
	   -W            $(v) prompt password
	   -y $(ul passwdfile) $(v)
	   -H $(ul ldapuri)    $(v)
	$(repeat_h 17)$(vh)$(fill_h 18)
	 $(gray)# Dry run modify$(reset)
	 $(green)ldapmodify $(cyan)-nv -f $(magenta)mod.ldif $(cyan)-x -D $(yellow)"${LDAP_BINDUSER}" $(cyan)-y $(magenta)/etc/ldap-password $(cyan)-H $(yellow)"${LDAP_URI}" $(reset)
	$(fill_h)
	 $(gray)# Dry run add$(reset)
	 $(green)ldapadd $(cyan)-nv -f $(magenta)mod.ldif $(cyan)-x -D $(yellow)"${LDAP_BINDUSER}" $(cyan)-y $(magenta)/etc/ldap-password $(cyan)-H $(yellow)"${LDAP_URI}" $(reset)
	$(fill_h)
	EOD
}

man_ldif_entry() {
	cat <<- EOD
	$(h)$(hv)$(fill_h 2)
	 $(v) Format $(ul ldif) for entries
	$(h)$(vh)$(fill_h 2)
	 $(gray)# Add entry$(reset)
	 dn: $(ul distinguished_name)
	 changetype: add
	 $(ul attribute): $(ul value)
	 ...
	$(fill_h)
	 $(gray)# Rename entry$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modrdn
	 newrdn: $(ul new_distinguished_name)
	 deleteoldrdn: $(ul '(0|1)')
	$(fill_h)
	 $(gray)# Delete entry$(reset)
	 dn: $(ul distinguished_name)
	 changetype: delete
	EOD
}

man_ldif_attribute() {
	cat <<- EOD
	$(h)$(hv)$(fill_h 2)
	 $(v) Format $(ul ldif) for entry attributes
	$(h)$(vh)$(fill_h 2)
	 $(gray)# Delete all attribute values$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modify
	 delete: $(ul attribute)
	 -
	 delete: $(ul attribute)
	$(fill_h)
	 $(gray)# Delete specific attribute values$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modify
	 delete: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	 -
	 delete: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	$(fill_h)
	 $(gray)# Add attributes (or attribute values)$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modify
	 add: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	 -
	 add: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	$(fill_h)
	 $(gray)# Modify all attribute values$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modify
	 replace: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	 -
	 replace: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	$(fill_h)
	 $(gray)# Modify specific attribute values$(reset)
	 dn: $(ul distinguished_name)
	 changetype: modify
	 delete: $(ul attribute)
	 $(ul attribute): $(ul value)
	 -
	 add: $(ul attribute)
	 $(ul attribute): $(ul value)
	 ...
	$(fill_h)
	EOD
}

case "${1}" in
	ldapsearch)
		man_ldapsearch; exit 0
		;;
	ldapmodify)
		man_ldapmodify; exit 0
		;;
	ldif)
		case "${2}" in
			entry)
				man_ldif_entry; exit 0
				;;
			attribute)
				man_ldif_attribute; exit 0
				;;
		esac
		;;
esac

cat <<EOD
Usage:
    ${0##*/} [(-f|--file) CONFIG] (ldapsearch|ldapmodify|ldif (entry|attribute))"
EOD
