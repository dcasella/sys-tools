# LDAP

LDAP specific administration tools and helpers.

## Scripts

### LDAP Man

```
ldap-man.sh
```

LDAP quick manual with examples.

#### Usage examples

```bash
./ldap-man.sh -f path/to/ldap.cfg ldapsearch
```

```bash
# default -f value: /etc/ldap.cfg
./ldap-man.sh ldif entry
```
