#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

# TODO: cleanup, usage, options

KEY="$1"
CERT="$2"
CACHAIN="$3"

if [ -z "$KEY" ] || [ -z "$CERT" ] || [ -z "$CACHAIN" ]; then
	echo "Please provide KEY CERT CACHAIN"
	exit 1
fi

echo "Verifying certificate validity"

/opt/zimbra/bin/zmcertmgr verifycrt comm "$KEY" "$CERT" "$CACHAIN" || exit 1

echo "Deploying certificate"

/opt/zimbra/bin/zmcertmgr deploycrt comm "$CERT" "$CACHAIN" || exit 1

echo "Verifying installed certificate"

/opt/zimbra/bin/zmcertmgr viewdeployedcrt || exit 1

read -p "Restart Zimbra? [y/N] " -n 1 -r

if [ -n "$REPLY" ]; then
	echo
fi

case $REPLY in
	y|Y)
		zmcontrol restart
		;;
esac
