# Zimbra

Zimbra specific administration tools.

## Scripts

### Certificate update

```
cert-update.sh
```

Perform a Zimbra certificate update (verify and deploy).

#### Download

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/Zimbra/cert-update.sh --output cert-update.sh; chmod +x cert-update.sh
```

#### Usage examples

```bash
./cert-update.sh /opt/zimbra/ssl/zimbra/commercial/commercial.key commercial.crt commercial_ca.crt
```
