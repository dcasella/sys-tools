#!/usr/bin/env python

# Original:
# https://github.com/gmelillo/tools/blob/master/setup_centos_7_minimal.py

from argparse import ArgumentParser
from logging import DEBUG, INFO, Formatter, StreamHandler, getLogger
from os import devnull, makedirs
from os.path import basename, expanduser, isdir
from subprocess import STDOUT, call
from sys import exit
from time import time

LOG_PATH = open(devnull, 'w')
SCRIPT_NAME = basename(__file__)
LOG_FOLDER = expanduser('~/.centos_7_setup')

logger = getLogger()
handler = StreamHandler()
formatter = Formatter(
    '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
)
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(INFO)


def set_log_file():
    '''
    Setup the log path and create the directory
    '''

    if not isdir(LOG_FOLDER):
        logger.info('Creating directory %s', LOG_FOLDER)
        makedirs(LOG_FOLDER)

    try:
        global LOG_PATH
        LOG_PATH = open(
            '{0}/{1}.log'.format(LOG_FOLDER, str(int(time()))), 'w+'
        )
    except IOError as error:
        logger.info('Error opening log file.')
        logger.info(str(error))


def exec_command(command):
    '''
    Exec shell commands
    '''

    return call(
        command,
        shell=True,
        stdout=LOG_PATH,
        stderr=STDOUT
    )


def setup_packages():
    '''
    Do the default customization and update packages
    '''

    logger.info('Updating packages')
    exec_command('yum -y update')

    logger.info('Installing tools')
    exec_command(
        'yum -y install '
        'wget postfix vim-enhanced bind-utils tcpdump lsof sysstat nmap iptraf ntp man screen bzip2'
    )

    logger.info('Disabling SELINUX and graphic boot')
    exec_command(
        'sed -i \'s/SELINUX=enforcing/SELINUX=permissive/\' /etc/selinux/config'
    )
    exec_command('sed -i \'s/rhgb//\' /boot/grub2/grub.cfg')
    exec_command('sed -i \'s/quiet//\' /boot/grub2/grub.cfg')

    logger.info('Configuring SSH service')
    exec_command(
        'sed -i \'s/#LogLevel INFO/LogLevel VERBOSE/\' /etc/ssh/sshd_config'
    )

    logger.info('Disabling firewall')
    exec_command('systemctl disable firewalld.service')

    logger.info('Configuring network')
    exec_command('nmcli')
    exec_command('systemctl restart NetworkManager.service')


def setup_logrotate(rotation):
    '''
    Map log files rotation to `rotate N` to preserve 2 years worth of logs
    '''

    rotate_map = {
        'daily': 730,
        'weekly': 104,
        'monthly': 24,
        'yearly': 2,
    }

    logger.info('Fixing logrotate')

    exec_command(
        'sed -i \''
        's/\\bweekly\\b/{0}/; '
        's/4 weeks/2 years/; '
        's/^rotate 4$/rotate {1}/; '
        's/^#compress$/compress\\n'
        'compresscmd \\/usr\\/bin\\/bzip2\\n'
        'uncompresscmd \\/usr\\/bin\\/bunzip2\\n'
        'compressoptions -9\\n'
        'compressext .bz2/'
        '\' /etc/logrotate.conf'
        .format(rotation, rotate_map[rotation])
    )


def disable_ipv6(eth):
    logger.info('Disabling IPv6')

    if eth is None:
        logger.error('Ethernet name not defined.')

        exit(2)
    else:
        exec_command(
            'sed -i \'s/restrict ::1/#restrict ::1/\' /etc/ntp.conf'
        )

        exec_command(
            'sed -i \'s/inet_protocols = all/inet_protocols = ipv4/\' /etc/postfix/main.cf'
        )

        exec_command(
            'echo \'NETWORKING_IPV6=no\' >> /etc/sysconfig/network'
        )

        exec_command('echo \'# Disables IPv6\' >> /etc/sysctl.conf')
        exec_command(
            'echo \'net.ipv6.conf.all.disable_ipv6 = 1\' >> /etc/sysctl.conf'
        )
        exec_command(
            'echo \'net.ipv6.conf.default.disable_ipv6 = 1\' >> /etc/sysctl.conf'
        )
        exec_command(
            'echo \'net.ipv6.conf.{0}.disable_ipv6 = 1\' >> /etc/sysctl.conf'
            .format(eth)
        )


def setup_puppet(server, environment):
    logger.info('Installing Puppet')
    exec_command(
        'rpm -Uvh https://yum.puppetlabs.com/puppet5/puppet5-release-el-7.noarch.rpm'
    )
    exec_command('yum install -y puppet-agent')
    exec_command('export PATH=$PATH:/opt/puppetlabs/bin')

    logger.info('Setting up Puppet')
    exec_command(
        'echo "[main]\n'
        '    logdir = /var/log/puppet\n'
        '[agent]\n'
        '    classfile   = $vardir/classes.txt\n'
        '    localconfig = $vardir/localconfig\n'
        '    server      = {0}\n'
        '    report      = true\n'
        '    splaylimit  = 1800\n'
        '    runinterval = 1800\n'
        '    pluginsync  = true\n'
        '    environment = {1}" > /etc/puppetlabs/puppet/puppet.conf\n'
        .format(server, environment)
    )

    logger.info('Enabling Puppet')
    exec_command(
        'puppet resource service puppet ensure=running enable=true'
    )


def main():
    set_log_file()

    parser = ArgumentParser()
    parser.add_argument('--vmware', dest='vmware', action='store_true',
                        help='Is virtual machine')
    parser.add_argument('--no-ipv6', dest='noipv6', action='store_true',
                        help='Disable IPv6 support')
    parser.add_argument('--eth', dest='eth', help='Ethetnet name')
    parser.add_argument('-l', '--logrotate', dest='logrotate',
                        choices=set(['daily', 'weekly', 'monthly', 'yearly']),
                        default='monthly',
                        help='Log files rotation')
    parser.add_argument('-p', '--puppet', dest='puppet', action='store_true',
                        help='Enable puppet install')
    parser.add_argument('--puppet-server', dest='puppetserver',
                        default='',
                        help='Set up Puppet server')
    parser.add_argument('--puppet-environment', dest='puppetenvironment',
                        default='',
                        help='Set up Puppet environment')
    parser.add_argument('--clean-kernel', dest='kernel', action='store_true',
                        help='Remove old kernels')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true',
                        help='Enable debug messages')

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(DEBUG)

    if args.kernel:
        logger.info('Cleaning up old kernels')
        exec_command('yum -y remove $(rpm -q kernel | grep -v $(uname -r))')

        logger.info('Kernels removed')

        exit(0)

    setup_packages()

    setup_logrotate(args.logrotate)

    exec_command('echo \'NOZEROCONF=yes\' >> /etc/sysconfig/network')

    if args.vmware:
        exec_command('yum -y install net-tools open-vm-tools')

    if args.noipv6:
        disable_ipv6(args.eth)

    if args.puppet:
        setup_puppet(args.puppetserver, args.puppetenvironment)

    logger.info('First configuration terminate.')
    logger.info('Reboot the system and then remove the old kernels with:')
    logger.info('\t%s --clean-kernel', SCRIPT_NAME)


if __name__ == '__main__':
    main()
