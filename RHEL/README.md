# RHEL

RHEL/CentOS specific administration tools.

## Scripts

### Setup

```
setup_centos7minimal.py
```

Initial setup for a minimal install of a CentOS 7 machine (physical or on VMware).

#### Usage examples

```bash
# VM, no IPv6 support, logrotate daily
./setup_centos7minimal.py --vmware --no-ipv6 --eth ethname --logrotate daily
```

```bash
# VM, IPv6 support, logrotate weekly
./setup_centos7minimal.py --vmware --logrotate weekly
```

```bash
# Physical, no IPv6 support, logrotate monthly
./setup_centos7minimal.py --no-ipv6 --eth ethname
```

```bash
# Physical, IPv6 support, logrotate monthly, puppet
./setup_centos7minimal.py --puppet --puppet-server puppetmasterfqdn --puppet-environment envname --puppet-host puppetmasterip
```
