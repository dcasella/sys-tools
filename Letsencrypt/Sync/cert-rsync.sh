#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

usage() {
	tee <<-EOD
	USAGE:
	    ${0##*/} [FLAG...] [OPTION...] REMOTE

	Sends a paramterized rsync command to REMOTE to sync the
	/etc/letsencrypt directory from REMOTE to local

	FLAGS:
	    -h                Display this message

	    All flags exit the program
	    Flags are parsed in the displayed order

	OPTIONS:
	    -d                Dry run, no actions will be performed

	    -v                Verbose, display performed operations

	PARAMETERS:
	    REMOTE            Remote IP address or FQDN
	EOD
}

rsync_letsencrypt() {
	local remote="$1"
	local dryrun="$2" verbose="$3"

	mkdir -p /etc/letsencrypt

	if [ -n "$verbose" ]; then
		echo 'Running key scan'
		echo "ssh-keyscan -t rsa $remote"
	fi

	remotekey="$(ssh-keyscan -t rsa "$remote")"

	if ! grep -q "$remotekey" ~/.ssh/known_hosts; then
		if [ -n "$verbose" ]; then
			echo 'Adding key to ~/.ssh/known_hosts'
		fi

		echo "$remotekey" >> ~/.ssh/known_hosts
	fi

	if [ -n "$verbose" ] && [ -n "$dryrun" ]; then
		echo 'Running rsync (verbose & dry-run)'
		echo "rsync -vvaunzih --no-motd --safe-links --delete --progress $remote:/etc/letsencrypt/ /etc/letsencrypt/"

		rsync -vvaunzih \
			--no-motd --safe-links --delete --progress \
			"$remote":/etc/letsencrypt/ /etc/letsencrypt/
	elif [ -n "$verbose" ]; then
		echo 'Running rsync (verbose)'
		echo "rsync -vvauzih --no-motd --safe-links --delete --progress $remote:/etc/letsencrypt/ /etc/letsencrypt/"

		rsync -vvauzih \
			--no-motd --safe-links --delete --progress \
			"$remote":/etc/letsencrypt/ /etc/letsencrypt/
	elif [ -n "$dryrun" ]; then
		echo 'Running rsync (dry-run)'
		echo "rsync -aunzih --no-motd --safe-links --delete $remote:/etc/letsencrypt/ /etc/letsencrypt/"

		rsync -aunzih \
			--no-motd --safe-links --delete \
			"$remote":/etc/letsencrypt/ /etc/letsencrypt/
	else
		echo >> /var/log/cert-rsync.log

		rsync -qauz \
			--no-motd --safe-links --delete --log-file=/var/log/cert-rsync.log \
			"$remote":/etc/letsencrypt/ /etc/letsencrypt/
	fi
}

main() {
	local dryrun verbose
	local remote

	while getopts ":hdv" opt; do
		case $opt in
		h)
			usage
			return 0
			;;
		d)
			dryrun=true
			;;
		v)
			verbose=true
			;;
		\?)
			echo "Option -$OPTARG is invalid. Use -h for help" >&2
			return 1
			;;
		:)
			echo "Option -$OPTARG requires an argument. Use -h for help" >&2
			return 1
			;;
		esac
	done
	shift $((OPTIND-1))

	if [ ${#@} == 0 ]; then
		echo "Remote IP/FQDN must be set. Use -h for help" >&2

		return 1
	fi

	remote="$1"

	rsync_letsencrypt "$remote" "$dryrun" "$verbose"
}

main "$@"
