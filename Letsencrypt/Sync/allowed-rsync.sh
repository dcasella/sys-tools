#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

case "$SSH_ORIGINAL_COMMAND" in
'rsync --server --sender -vvunlogDtprze.iLsfxC --safe-links . /etc/letsencrypt/')
	rsync --server --sender -vvunlogDtprze.iLsfxC --safe-links . /etc/letsencrypt/
	;;
'rsync --server --sender -vvulogDtprze.iLsfxC --safe-links . /etc/letsencrypt/')
	rsync --server --sender -vvulogDtprze.iLsfxC --safe-links . /etc/letsencrypt/
	;;
'rsync --server --sender -unlogDtprze.iLsfxC --safe-links . /etc/letsencrypt/')
	rsync --server --sender -unlogDtprze.iLsfxC --safe-links . /etc/letsencrypt/
	;;
'rsync --server --sender -ulogDtprze.iLsfxC --safe-links . /etc/letsencrypt/')
	rsync --server --sender -ulogDtprze.iLsfxC --safe-links . /etc/letsencrypt/
	;;
*)
	echo "Access denied" >&2
	exit 1
	;;
esac
