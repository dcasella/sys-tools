# Letsencrypt

Letsencrypt certificates specific administration tools.

## Scripts

### Sync

```
cert-rsync.sh
```

Sends a parameterized `rsync` command to the specified remote (IP/FQDN), synchronizing the `/etc/letsencrypt` directory from remote to local.

```
allowed-rsync.sh
```

Limits SSH connection to an array of allowed `rsync` commands.

#### Download

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/Letsencrypt/Sync/cert-rsync.sh --output cert-rsync; chmod +x cert-rsync
```

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/Letsencrypt/Sync/allowed-rsync.sh --output allowed-rsync; chmod +x allowed-rsync
```

#### Installation

Example setup:

```
host-back1
       \
        host-front
       /
host-back2
```

Configuration:

- Generate a SSH key pair `id_rsa` and `id_rsa.pub`

- host-back1:
  - `/path/to/cert-rsync` is `cert-rsync.sh`
  - `/path/to/user/.ssh/id_rsa` is `id_rsa`
- host-back2:
  - `/path/to/cert-rsync` is `cert-rsync.sh`
  - `/path/to/user/.ssh/id_rsa` is `id_rsa`
- host-front:
  - `/path/to/allowed-rsync` is `allowed-rsync.sh`
  - `/path/to/user/.ssh/authorized_keys` contains `id_rsa.pub`
  - Prepend `command="/absolute/path/to/allowed-rsync"` to the configured SSH authorized key

#### Usage examples

```bash
./cert-rsync -v root@remote-server
```
