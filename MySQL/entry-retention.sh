#!/usr/bin/env bash

set -o pipefail # non-zero exit codes propagate to the end of a pipeline

usage() {
	tee <<-EOD
	USAGE:
	    ${0##*/} [FLAG...] [OPTION...]

	Deletes entries whose date field is more than MONTHS months old from every
	table available in the selected DATABASE

	FLAGS:
	    -h                Display this message

	    All flags exit the program
	    Flags are parsed in the displayed order

	OPTIONS:
	    -b DATABASE       MySQL --database parameter
	                      Required

	    -s HOST           MySQL -h/--host parameter
	                      Default: 127.0.0.1

	    -u USER           MySQL -u/--user parameter
	                      Default: root

	    -p PASSWORD       MySQL -p/--password parameter
	                      Default: None

	    -m MONTHS         Number of months (MySQL INTERVAL)
	                      Default: 6

	    -d                Dry run, SELECT * instead of DELETE

	PARAMETERS:
	    None
	EOD
}

main() {
	local dryrun
	local database host='127.0.0.1' user='root' password months=6

	while getopts ":hb:s:u:p:m:d" opt; do
		case $opt in
		h)
			usage
			return 0
			;;
		b)
			database="$OPTARG"
			;;
		s)
			host="$OPTARG"
			;;
		u)
			user="$OPTARG"
			;;
		p)
			password="$OPTARG"
			;;
		m)
			months="$OPTARG"
			;;
		d)
			dryrun=true
			;;
		\?)
			echo "Option -$OPTARG is invalid. Use -h for help" >&2
			return 1
			;;
		:)
			echo "Option -$OPTARG requires an argument. Use -h for help" >&2
			return 1
			;;
		esac
	done
	shift $((OPTIND-1))

	if [ -z "$database" ]; then
		echo "MySQL database must be set. Use -h for help" >&2

		return 1
	fi

	if [[ ! $months =~ ^-?[0-9]+$ ]]; then
		echo "Months must be an integer. Use -h for help" >&2

		return 1
	fi

	tables="$(\
		mysql --host="$host" \
		      --user="$user" \
		      --password="$password" \
		      --database="$database" \
		      --execute='show tables;' |
			tail --lines=+2)"

	for table in $tables; do
		local query

		echo "Current table: $table"

		if [ -n "$dryrun" ]; then
			read -r -d '' query <<-EOD
			SELECT *
			FROM $table
			WHERE date < NOW() - INTERVAL $months MONTH
			EOD
		else
			read -r -d '' query <<-EOD
			DELETE
			FROM $table
			WHERE date < NOW() - INTERVAL $months MONTH
			EOD
		fi

		mysql --host="$host" \
		      --user="$user" \
		      --password="$password" \
		      --database="$database" \
		      --execute="$query;"
	done
}

main "$@"
