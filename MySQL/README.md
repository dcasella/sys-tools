# MySQL

MySQL specific administration tools.

## Scripts

### Entry retention

```
entry-retention.sh
```

Deletes entries whose date field is more than the specified months old from every table available in the selected database.

#### Download

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/MySQL/entry-retention.sh --output entry-retention.sh; chmod +x entry-retention.sh
```

#### Usage examples

```bash
# mysql -h wela.eu-west-1.rds.amazonaws.com -u notroot -p...
# delete entries more than 2 months old
./entry-retention.sh -b rsyslog_db -s wela.eu-west-1.rds.amazonaws.com -u notroot -p "$(cat /root/mysql-notroot-password)" -m 2
```

```bash
# mysql -h wela.eu-west-1.rds.amazonaws.com -u root -p
# select (dry-run) entries more than 6 months old
./entry-retention.sh -b rsyslog_db -s wela.eu-west-1.rds.amazonaws.com -d
```
