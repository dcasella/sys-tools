#!/usr/bin/env bash

read -p "MySQL root password: " -rs
echo
test -z "$REPLY" && exit 1

MYSQL_ROOT_PASSWORD="$REPLY"
unset REPLY

read -p "Bacula server user@host: " -r
test -z "$REPLY" && exit 1

SERVER_SSH="$REPLY"
unset REPLY

read -p "Bacula server user password: " -rs
echo
test -z "$REPLY" && exit 1

SERVER_PASSWORD="$REPLY"
unset REPLY

printf 'Stopping services... '
systemctl stop bacula-{dir,sd,fd} &&
echo 'Done'

printf 'Dumping server database... '
sshpass -p "$SERVER_PASSWORD" ssh "$SERVER_SSH" \
	"mysqldump -u root -p${MYSQL_ROOT_PASSWORD} bacula > /root/bacula_dump.sql" &&
echo 'Done'

printf 'Dumping and dropping current database... '
mysqldump -u root -p"$MYSQL_ROOT_PASSWORD" bacula > /root/pre_bacula_dump.sql &&
mysql -u root -p"$MYSQL_ROOT_PASSWORD" \
	-e 'drop database bacula; create database bacula' &&
echo 'Done'

printf 'Retrieving server database... '
sshpass -p "$SERVER_PASSWORD" \
	scp "$SERVER_SSH":/root/bacula_dump.sql . &&
echo 'Done'

printf 'Importing server database... '
mysql -u root -p"$MYSQL_ROOT_PASSWORD" bacula < bacula_dump.sql &&
echo 'Done'

printf 'Updating imported database... '
yes "$MYSQL_ROOT_PASSWORD" |
	/usr/libexec/bacula/update_mysql_tables -u root -p"$MYSQL_ROOT_PASSWORD" > /dev/null &&
echo 'Done'

printf 'Starting services... '
systemctl start bacula-{dir,sd,fd} &&
echo 'Done'
