# Bacula

Bacula specific administration tools.

## Scripts

### MySQL database update

```
mysql-update.sh
```

Perform a Bacula database update from a different Bacula backup server.

#### Download

```bash
curl https://gitlab.com/dcasella/sys-tools/raw/master/Bacula/mysql-update.sh --output mysql-update.sh; chmod +x mysql-update.sh
```

#### Usage

```bash
./mysql-update.sh
```
