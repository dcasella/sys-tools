#!/usr/bin/env bash

set -o errexit  # exit script when a command fails
set -o pipefail # non-zero exit codes propagate to the end of a pipeline

usage() {
	tee <<-EOD
	USAGE:
	    ${0##*/} [FLAG...] [OPTION...] FILE

	Manages FILE

	FLAGS:
	    -h                Display this message

	    All flags exit the program
	    Flags are parsed in the displayed order

	OPTIONS:
	    -p IP=FQDN        Manage the IP - FQDN assignment
	                      Required, multiple -p instances allowed
	                      (ex. "10.1.2.3=host1"
	                           "10.1.4.5=host2.domain.name")

	    -r                Replace inconsistent entries

	    -d                Dry run, no actions will be performed

	    -v                Verbose, display performed operations

	PARAMETERS:
	    FILE              Path to the hosts file
	EOD
}

main() {
	local pairs=() replace dryrun verbose
	local filepath

	while getopts ":hp:rdv" opt; do
		case $opt in
		h)
			usage
			return 0
			;;
		p)
			pairs+=("$OPTARG")
			;;
		r)
			replace=true
			;;
		d)
			dryrun=true
			;;
		v)
			verbose=true
			;;
		\?)
			echo "Option -$OPTARG is invalid. Use -h for help" >&2
			return 1
			;;
		:)
			echo "Option -$OPTARG requires an argument. Use -h for help" >&2
			return 1
			;;
		esac
	done
	shift $((OPTIND-1))

	if [ ${#@} == 0 ]; then
		echo "File path must be set. Use -h for help" >&2

		return 1
	fi

	filepath="$1"

	if [ ! -f "$filepath" ]; then
		echo "File $filepath is not valid. Use -h for help" >&2

		return 1
	fi

	if [ -z "${pairs[*]}" ]; then
		echo "Option -p must be set at least once. Use -h for help" >&2

		return 1
	fi

	local ips fqdns hosts
	local ipmaxlen=0 fqdnmaxlen=0
	local replaceline

	# for each IP=FQDN pair
	for pair in "${pairs[@]}"; do
		local pairsplit
		IFS='=' read -ra pairsplit <<< "$pair"

		local ip fqdn host
		ip="${pairsplit[0]}"
		fqdn="${pairsplit[1]}"
		host="${fqdn%%.*}"

		# skip invalid IP=FQDN (empty IP, empty FQDN, or not pair)
		if [ -z "$ip" ] ||
		   [ -z "$fqdn" ] ||
		   [ ${#pairsplit[@]} -ne 2 ]; then
			if [ -n "$verbose" ]; then
				echo "Pair \"$pair\" is invalid, skipping" >&2
			fi

			continue
		fi

		# update IP padding
		if [ ${#ip} -gt $ipmaxlen ]; then
			ipmaxlen=${#ip}
		fi

		# update FQDN padding
		if [ ${#fqdn} -gt $fqdnmaxlen ]; then
			fqdnmaxlen=${#fqdn}
		fi

		local linenumbers=()

		# find IP line numbers in file
		while IFS= read -r line; do
			linenumbers+=("${line%%:*}")
		done <<< "$(grep -nw "$ip" "$filepath" || true)"

		for ((i=0; i<${#ips[@]}; ++i)); do
			if [ "${ips[i]}" != "$ip" ]; then
				continue
			fi

			# IP found: add FQDN and Host
			fqdns[$i]="${fqdns[i]},$fqdn"
			hosts[$i]="${hosts[i]},$host"

			# then skip outer loop cycle
			continue 2
		done

		# new IP, new life, new FQDN, new Host
		ips+=("$ip")
		fqdns+=("$fqdn")
		hosts+=("$host")

		# new line numbers list, if IP was found in file
		if [ -n "$replace" ] && [ -n "${linenumbers[*]}" ]; then
			IFS=',' replaceline+=("${linenumbers[*]}")
		else
			replaceline+=('')
		fi
	done

	# for each IP
	for ((i=0; i<${#ips[@]}; ++i)); do
		local fqdnlist hostlist replacelist
		IFS=',' read -ra fqdnlist <<< "${fqdns[i]}"
		IFS=',' read -ra hostlist <<< "${hosts[i]}"
		IFS=',' read -ra replacelist <<< "${replaceline[i]}"

		# for each FQDN assigned to IP
		for ((j=0; j<${#fqdnlist[@]}; ++j)); do
			local line
			line=$(\
				printf "%-*s %-*s %s\\n" \
					"$ipmaxlen" "${ips[i]}" \
					"$fqdnmaxlen" "${fqdnlist[j]}" \
					"${hostlist[j]}")

			if [ -n "${replacelist[j]}" ]; then
				# replace line (where the IP was found) with updated FQDN and Host
				# if dry-run is disabled
				if [ -z "$dryrun" ]; then
					sed -i "${replacelist[j]}s/.*/$line/" "$filepath"
				fi

				if [ -n "$verbose" ]; then
					echo -n 'Replace: '
				fi
			else
				# add line with IP, FQDN and Host if dry-run is disabled
				if [ -z "$dryrun" ]; then
					echo "$line" >> "$filepath"
				fi

				if [ -n "$verbose" ]; then
					echo -n 'Add:     '
				fi
			fi

			if [ -n "$verbose" ]; then
				echo "$line (L${replacelist[j]})"
			fi
		done
	done
}

main "$@"
